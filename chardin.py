#!/usr/bin/python
#*-coding:Utf-8 -*

###############################################################################
# Meta
##############################################################################

# Chardin Anonymous Messaging Bot
# Author : Lucas Vincent lucas.vincent@entalpi.net
# Version : 0.12


###############################################################################
# Dépendances
###############################################################################

import discord
import asynci
import requests, random, os
import json

from discord.ext import commands

###############################################################################
# Configuration
###############################################################################

configpath = "./.config/config"

###############################################################################
# Déclaration de la classe du bot.
###############################################################################

class Mybot(commands.Bot):
    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')

###############################################################################
# Construction de l'objet.
###############################################################################

chardin = Mybot(command_prefix='?', description='Send anonymous messages with this marvelous bot')

###############################################################################
# Déclaration des commandes du bot.
##############################################################################

#Envoie de messages anonymes.
@chardin.command()
async def anon(ctx,channelname,guildname,*args):

    #On accepte exclusivement les commandes qui proviennent des DM.
    if(ctx.channel.type == discord.ChannelType.private):
        try:
            #Les arguments en queue sont le message à transmettre.
            message = ' '.join(args)

            #Confirmer la tentative d'envoi
            await ctx.send("I'm sending anonymous message to {} on {} : \"".format(channelname,guildname)+message+"\"…")

            #Récupérer l'objet canal dans lequel il faut envoyer le message.
            channel = discord.utils.get(chardin.get_all_channels(), guild__name=guildname, name=channelname)

            #L'envoyer.
            await channel.send("*Un anonyme dit :*")
            await channel.send("« "+message+" »")

            #Confirmation à l'expéditeur.
            await ctx.send("Done.")
        except:

            #Message d'erreur, par exemple si le canal ou le serveur n'existe pas.
            #On pourra discriminer les différentes erreurs possible plus tard pour
            #rendre les messages plus explicites.
            await ctx.send("error executing the command.")
    else:
        await ctx.send("No.")
    return

###############################################################################
# Lançeur du bot.
###############################################################################

# Chopper le chemin absolu du fichier de configuration
loc = os.path.dirname(__file__)
abs_configpath = os.path.join(loc, configpath)

# On en récupère le contenu au format JSON.
configfile = open(abs_configpath, 'r') 
configobj = json.load(configfile)
configfile.close()

#lancer le bot avec le token
chardin.run(tokenobj['token'])
